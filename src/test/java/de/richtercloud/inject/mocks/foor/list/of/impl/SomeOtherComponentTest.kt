package de.richtercloud.inject.mocks.foor.list.of.impl

import io.mockk.MockKAnnotations
import io.mockk.impl.annotations.InjectMockKs
import io.mockk.impl.annotations.MockK
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class SomeOtherComponentTest {
    @MockK
    lateinit var someComponentImpl0: SomeComponentImpl0

    @MockK
    lateinit var someComponentImpl1: SomeComponentImpl1

    @InjectMockKs
    lateinit var instance: SomeOtherComponent

    @BeforeEach
    fun setup() {
        MockKAnnotations.init(this)
    }

    @Test
    fun testSomething() {
        println(instance.impls.toString())
    }
}