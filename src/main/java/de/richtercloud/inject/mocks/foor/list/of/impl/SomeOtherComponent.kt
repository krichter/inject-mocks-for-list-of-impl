package de.richtercloud.inject.mocks.foor.list.of.impl

import org.springframework.beans.factory.annotation.Autowired

class SomeOtherComponent {
    @Autowired
    lateinit var impls: List<SomeComponent>
}
